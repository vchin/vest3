import traceback
from collections import OrderedDict
import hashlib
import os
import urllib
# import urlparse
import sys
import re
import codecs

from datetime import datetime, timezone
from uuid import uuid4
from copy import deepcopy
from importlib import import_module

import ujson
import transliterate

from django.forms import model_to_dict
from django.utils.text import slugify
from django.contrib import auth
from django.core.files.base import ContentFile
from django.apps import apps
from django.http import HttpResponse
from django.template.loader import get_template
from django.utils.deconstruct import deconstructible
from django.utils.translation import get_language
from django.conf import settings
from builtins import str

import logging

log = logging.getLogger('vest_utils')


def file_put_contents(filename, data, utf=False):
    f = codecs.open(filename, "w", "utf-8-sig") if utf else open(filename, 'w')
    f.write(data)
    f.close()


def file_get_contents(url):
    if os.path.exists(url):
        return open(url, 'r').read()

    opener = urllib.request.build_opener()
    content = opener.open(url).read()
    return content


def image_from_url_get(url):
    url_data = urllib.parse.urlparse(url)

    ext = os.path.splitext(url_data.path)[1]
    return ContentFile(urllib.request.urlopen(url).read())


def image_from_url_get_2(url, name_gen=True):
    file = image_from_url_get(url)
    ext = os.path.splitext(urllib.parse.urlparse(url).path)[1]
    if name_gen:
        md5 = hashlib.md5()
        # md5.update('%s-%s' % (url, datetime.now()))
        md5.update('{}-{}'.format(url, datetime.now()).encode('utf-8'))
        if not ext:
            ext = '.jpg'

        file.name = '%s%s' % (md5.hexdigest(), ext.lower())
    else:
        file.name = os.path.basename(url)

    return file


def md5_for_file(f, block_size=2 ** 20):
    f = open(f)
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)

    f.close()
    return md5


def model_class_get(path):
    return apps.get_model(*path.split('.'))


def model_object_get(path, pk):
    return model_class_get(path).objects.get(pk=pk)


def class_get(path):
    arr = path.split('.')
    return getattr(import_module('.'.join(arr[0:-1])), arr[-1])


def qset_to_dict(qset, keys='id', out=None):
    res = OrderedDict()

    if not isinstance(keys, list):
        keys = [keys]

    for item in qset:
        for key in keys:
            key_name = key(item) if hasattr(key, '__call__') else getattr(item,
                key)
            res[key_name] = out(key, item) if out else item

    return res


def vt_model_to_dict(instance, deep_level=100, fields=None, exclude=None):
    dc = {}
    simple_fields = []

    for fld in instance._meta.fields:
        if not exclude is None and fld.name in exclude:
            continue

        if not fields is None and not fld.name in fields:
            continue

        if fld.is_relation:
            value = getattr(instance, fld.name)
            if value is None:
                dc[fld.name] = None
            elif deep_level - 1 >= 0:
                dc[fld.name] = vt_model_to_dict(value, deep_level - 1)
            else:
                dc[fld.name] = str(dc[fld.name])
        else:
            simple_fields.append(fld.name)

    if len(simple_fields):
        dc.update(model_to_dict(instance, simple_fields))

    return dc


def shop_login(request, username, password):
    if auth.SESSION_KEY in request.session:
        del request.session[auth.SESSION_KEY]
        request.session.modified = True

    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
    return user


def exception_details():
    exc_type, exc_obj, exc_tb = sys.exc_info()
    file_name = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    return file_name, exc_type, exc_obj, exc_tb


def dict_sort(dic_or_list, key):
    if isinstance(dic_or_list, list):
        sorted_x = sorted(dic_or_list, key=lambda x: x[key])
    else:
        sorted_x = sorted(dic_or_list.items(), key=lambda x: x[1][key])
    return sorted_x


def handle_uploaded_file(f, path):
    destination = open(path, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()


def file_exists(path):
    try:
        with open(path) as f:
            return True
    except IOError as e:
        return None


# def template_to_source():
# loader = Loader()
#
#     apps_root = os.path.realpath('%s/../' % settings.PROJECT_ROOT)
#
#     for st in SiteTemplate.active_objects.all():
#         for filepath in loader.get_template_sources(st.name):
#             try:
#                 if file_exists(filepath) and filepath.startswith(apps_root):
#                     with codecs.open(filepath, 'w', 'utf8') as f:
#                         f.write(st.content)
#                         print st.name, filepath, '-ok'
#             except IOError as e:
#                 print e


first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')


def camel_to_underline(name):
    s1 = first_cap_re.sub(r'\1_\2', name)
    return all_cap_re.sub(r'\1_\2', s1).lower()


'''

<?xml version="1.0" encoding="UTF-8"?>
 <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
 {% for item in items %}
     {% if item.url|first == '/' %}
    <url>
        <loc>{{ item.url }}</loc>
        <changefreq>weekly</changefreq>
        <priority>1</priority>
    </url>
    {% endif %}
{% endfor %}
</urlset>

'''


def site_map_generate(**kwargs):
    urls = kwargs['urls']  # generator
    path = kwargs['path']  # sitemap.xml path
    domain = kwargs['domain']
    sub_path = kwargs.get('sub_path', '')

    max_links = kwargs.get('max_links', 50000)

    parts = 1

    def _xfi():
        file_name = 'sitemap{}.xml'.format(parts)
        file_path = os.path.join(path, file_name)
        file = codecs.open(file_path, 'w+')

        xml_start = '''<?xml version="1.0" encoding="UTF-8"?> 
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'''
        file.write(xml_start)
        return file

    file = _xfi()

    for i, url in enumerate(urls, 1):
        file.write('<url>')
        for tag in ['loc', 'changefreq', 'priority', 'lastmod']:
            if tag in url:
                file.write('<{0}>{1}</{0}>'.format(tag, url[tag]))

        file.write('</url>')

        if i % max_links == 0:
            file.write('</urlset>')
            file.close()
            parts += 1
            file = _xfi()

    file.write('</urlset>')
    file.close()

    if parts == 1:
        os.rename(os.path.join(path, 'sitemap1.xml'),
            os.path.join(path, 'sitemap.xml'))
    else:
        # iso_date = datetime.now().isoformat()
        iso_date = datetime.utcnow().replace(microsecond=0).replace(
            tzinfo=timezone.utc).isoformat()
        with codecs.open(os.path.join(path, 'sitemap.xml'), 'w+', 'utf-8') as f:
            f.write(
                '''<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">''')
            for i in range(parts):
                f.write('''<sitemap>
<loc>{}/{}sitemap{}.xml</loc>
<lastmod>{}</lastmod>
</sitemap>'''.format(domain, sub_path, i + 1, iso_date))
            f.write('''</sitemapindex>''')


# TODO: DEPRECATED!!!
class SiteMapGenerator(object):
    def __init__(self):
        self.file = None

    def _write_header(self):
        self.file.write('''<?xml version="1.0" encoding="UTF-8"?>''')

    def _open_urlset(self):

        self.file.write(
            '''<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">''')

    def _close_urlset(self):
        self.file.write('</urlset>')

    def _write_urls(self, urls):
        for url in urls:
            self.file.write('<url>')
            for tag in ['loc', 'changefreq', 'priority', 'lastmod']:
                if tag in url:
                    self.file.write('<{0}>{1}</{0}>'.format(tag, url[tag]))
            self.file.write('</url>')

    def generate(self, path, **kwargs):
        try:
            self.file = open(path, 'w+')
            self._write_header()
            self._open_urlset()
            self._write_urls(kwargs['urls'])
            self._close_urlset()

        except BaseException as e:
            ex = traceback.format_exc()
            log.error(ex)
            return {'success': False, 'error': ex}
        finally:
            self.file.close()
        return {'success': True}


def dict_merge(target, *args):
    # Merge multiple dicts
    if len(args) > 1:
        for obj in args:
            dict_merge(target, obj)
        return target

    # Recursively merge dicts and set non-dict values
    obj = args[0]
    if not isinstance(obj, dict):
        return obj
    for k, v in obj.items():
        if k in target and isinstance(target[k], dict):
            dict_merge(target[k], v)
        else:
            target[k] = deepcopy(v)
    return target


# request.FILES['file']
# path = 'users/1' for example
from django.core.files.storage import default_storage


def form_file_save(file, path):
    name, ext = os.path.splitext(file.name)
    name = '%s%s' % (str(uuid4()), ext)
    file_path = '%s/%s' % (path, name)

    full_path = default_storage.save(file_path, ContentFile(file.read()))
    return file_path, full_path


@deconstructible
class UploadPath(object):
    def __init__(self, *args, **kwargs):
        self.base_path = kwargs['base_path']
        self.field_name = kwargs.get('field_name')
        self.name_gen = kwargs.get('name_gen')

    def __call__(self, instance, file_name):
        name = file_name

        if self.name_gen:
            name, ext = os.path.splitext(file_name)
            name = '%s%s' % (str(uuid4()), ext)

        if self.field_name:
            field = getattr(instance, self.field_name)
            path = os.path.join(self.base_path,
                field.slug if hasattr(field, 'slug') else str(
                    field.pk))
        else:
            path = self.base_path

        full_path = os.path.join(settings.MEDIA_ROOT, path)
        if not os.path.exists(full_path):
            os.makedirs(full_path)

        path = os.path.join(path, name)

        return path
        # return path.format(instance.user.username, self.sub_path, filename)


def lang_get():
    lng = get_language()
    if not lng is None:
        return get_language().split('-')[0]

    return 'ru'


def slugify_ru(in_str, language_code=None):
    try:
        return slugify(
            transliterate.translit(in_str, language_code=language_code,
                reversed=True))
    except Exception as e:
        return slugify(
            transliterate.translit(str(in_str), language_code=language_code,
                reversed=True))


def ujson_response(x):
    return HttpResponse(ujson.dumps(x),
        content_type='application/json; charset=UTF-8')


def ex_find_template(name, exclude=[], dirs=None):
    using = None
    if len(exclude):
        using = []
        for loader_name in settings.TEMPLATE_LOADERS:
            if loader_name in exclude:
                continue
            using.append(loader_name)

    return get_template(name)


# language prefix
vt_lang_prefix = lambda url: r'^([A-Za-z]{2}/)?%s' % url

vt_kw_lang_prefix = lambda url: r'^(?P<lng>[A-Za-z]{2}/)?%s' % url
