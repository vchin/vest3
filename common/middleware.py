import urllib
import re

from django import http
from django.urls import resolve

from django.shortcuts import render
from django.utils.deprecation import MiddlewareMixin
from django.middleware.locale import LocaleMiddleware
from django.utils import translation
from django.conf import settings
from six import iteritems

from common.thread_locals import get_current_site
from .thread_locals import set_thread_var
from .cachedtree import CacheTree


class MiddlewareInfoView(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_view(self, request, view_func, view_args, view_kwargs):
        try:
            resolver = resolve(request.path)

            request.vt_view = {
                'resolver': resolver,
                'view_func_name': view_func.__name__,
                'path': request.path,
                'namespace': resolver.namespace,
                'view_name': resolver.view_name,
                'short_view_name': resolver.view_name.split(':')[-1],

                'view_func': view_func,
                'view_args': view_args,
                'view_kwargs': view_kwargs,

            }
        except BaseException as e:
            request.vt_view = {}


class MiddlewareMultipleProxy(MiddlewareMixin):
    FORWARDED_FOR_FIELDS = [
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED_HOST',
        'HTTP_X_FORWARDED_SERVER',
    ]
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        """
        Rewrites the proxy headers so that only the most
        recent proxy is used.
        """
        for field in self.FORWARDED_FOR_FIELDS:
            if field in request.META:
                if ',' in request.META[field]:
                    parts = request.META[field].split(',')
                    request.META[field] = parts[-1].strip()


class MiddlewareThreadLocal(MiddlewareMixin):
    """ Simple middleware that adds the request object in thread local storage."""

    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        set_thread_var('request', request)


class MiddlewareAjaxCSRFDisable(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)


class MiddlewareFilterPersist(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):

        if '/admin/' not in request.path:
            return None

        if not request.META.has_key('HTTP_REFERER'):
            return None

        popup = 'pop=1' in request.META['QUERY_STRING']
        path = request.path
        query_string = request.META['QUERY_STRING']
        session = request.session

        if session.get('redirected',
                False):  # so that we dont loop once redirected
            del session['redirected']
            return None

        referrer = request.META['HTTP_REFERER'].split('?')[0]
        referrer = referrer[referrer.find('/admin'):len(referrer)]
        key = 'key' + path.replace('/', '_')
        if popup:
            key = 'popup' + path.replace('/', '_')

        if path == referrer:  # We are in same page as before
            if query_string == '':  # Filter is empty, delete it
                if session.get(key, False):
                    del session[key]
                return None
            # request.session[key] = query_string
            request.session[key] = request.GET.copy()
            request.session.modified = True
        else:  # We are are coming from another page, restore filter if available
            if session.get(key, False):
                #                urllib.urlencode
                try:
                    query = request.session.get(key)
                    query.update(request.GET.copy())
                    query_string = urllib.urlencode(query)
                    redirect_to = path + '?' + query_string
                    request.session['redirected'] = True
                    return http.HttpResponseRedirect(redirect_to)
                except:
                    pass
                    #                query_string=request.session.get(key)

        return None


class MiddlewareSite(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        request.site = get_current_site()


class MiddlewareAppData(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        from django.conf import settings

        request.APP_DATA = settings.APP_DATA


# operate lang switch by url
class MiddlewareLang(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        from django.conf import settings

        langs = dict(settings.LANGUAGES)
        lang = settings.LANGUAGE_CODE.split('-')[0]

        path = request.path.split('/')
        if len(path) > 1 and path[1] in langs:
            request.lang = path[1]
        else:
            request.lang = lang


class MiddlewareSwitchLocale(LocaleMiddleware):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        #        if 'language' in request.GET:
        #            request.session['django_language'] = request.GET['language']
        language = request.lang
        translation.activate(language)
        request.LANGUAGE_CODE = language


class MiddlewareLocale(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        sr = re.search('^/([A-Za-z]{2})/', request.path)
        lang_dc = dict(item for item in settings.LANGUAGES)

        if sr:
            request.vt_lang = sr.group(1) if sr and sr.group(
                1) in lang_dc else settings.LANGUAGE_CODE
        else:
            lng = request.META.get('HTTP_ACCEPT_LANGUAGE', '')
            request.vt_lang = lng if lng and lng in lang_dc \
                else settings.LANGUAGE_CODE

        translation.activate(request.vt_lang)


# for django admin
# DEPRECATED!!!
class MiddlewareFilterPersist(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        if '/admin/' not in request.path:
            return None

        if not request.META.has_key('HTTP_REFERER'):
            return None

        popup = 'pop=1' in request.META['QUERY_STRING']
        path = request.path

        if path.find('/add/') >= 0:
            return None

        query_string = request.META['QUERY_STRING']
        session = request.session

        if session.get('redirected',
                False):  # so that we dont loop once redirected
            del session['redirected']
            return None

        referrer = request.META['HTTP_REFERER'].split('?')[0]
        referrer = referrer[referrer.find('/admin'):len(referrer)]
        key = 'key' + path.replace('/', '_')
        if popup:
            key = 'popup' + path.replace('/', '_')

        if path == referrer:  # We are in same page as before
            if query_string == '':  # Filter is empty, delete it
                if session.get(key, False):
                    del session[key]
                return None
            # request.session[key] = query_string
            request.session[key] = request.GET.copy()
            request.session.modified = True
        else:  # We are are coming from another page, restore filter if available
            if session.get(key, False):
                #                urllib.urlencode
                try:
                    query = request.session.get(key)
                    query.update(request.GET.copy())
                    query_string = urllib.urlencode(query)
                    redirect_to = path + '?' + query_string
                    request.session['redirected'] = True
                    return http.HttpResponseRedirect(redirect_to)
                except BaseException as e:
                    pass
                    #                query_string=request.session.get(key)

        return None


class AbstractMiddlewareSimplePage(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request, cls, context=None):
        try:
            if not hasattr(request, 'simple_page'):
                request.simple_page = None

            url = re.sub(r'/+', '/', '/%s/' % request.path_info.strip('/'),
                re.IGNORECASE)
            sps = []
            # check_re

            # check if this seller subdomain
            qset = cls.objects.filter(url=url, state=True)

            for idx, sp in enumerate(qset):
                sps.append(sp)

            if len(sps):
                request.sps = sps

                return render(request, 'apps/frontend/simple_page_middleware.j2')

        except cls.DoesNotExist:
            request.simple_page = None


class MiddlewareRequest(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        # Im not happy on new django changes for future
        request.REQUESTS = {}
        request.REQUESTS.update(request.POST)
        request.REQUESTS.update(request.GET)

        for key, val in iteritems(request.REQUESTS):
            if isinstance(val, list) and len(val) == 1:
                request.REQUESTS[key] = val[0]


class MiddlewareCachedTree(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        from django.apps import apps
        cls = apps.get_model(settings.CATEGORY_MODEL)

        request.cached_tree = CacheTree(cls.tree_get(), request.path)
