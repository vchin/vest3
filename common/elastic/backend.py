# -*- coding: utf-8 -*-
from haystack.backends.elasticsearch_backend import ElasticsearchSearchBackend, \
    ElasticsearchSearchEngine
from haystack.backends import elasticsearch_backend

elasticsearch_backend.FIELD_MAPPINGS['ru_en_field'] = {'type': 'string',
                                                       'analyzer': 'ru_en'}

# FIELD_MAPPINGS

elasticsearch_backend.FIELD_MAPPINGS['ngram'] = {'type': 'string',
                                                 'analyzer': 'str_index_analyzer',
                                                 'search_analyzer': 'str_search_analyzer'}


#

class CustomElasticsearchBackend(ElasticsearchSearchBackend):
    def __init__(self, connection_alias, **connection_options):
        self.DEFAULT_SETTINGS['settings']['analysis']['analyzer'][
            'str_search_analyzer'] = {
            'tokenizer': 'whitespace',
            'filter': ['lowercase']
        }

        self.DEFAULT_SETTINGS['settings']['analysis']['analyzer'][
            'str_index_analyzer'] = {
            'tokenizer': 'keyword',
            'filter': ['lowercase', 'substring']
        }

        self.DEFAULT_SETTINGS['settings']['analysis']['analyzer'][
            'ru_en'] = {
            'type': 'custom',
            'tokenizer': 'standard',
            'filter': ['lowercase', 'russian_morphology', 'english_morphology',
                       'ru_en_stopwords']
        }

        self.DEFAULT_SETTINGS['settings']['analysis']['filter']['substring'] = {
            'type': 'nGram',
            'min_gram': 3,
            'max_gram': 20
        }

        self.DEFAULT_SETTINGS['settings']['analysis']['filter'][
            'ru_en_stopwords'] = {
            'type': 'stop',
            'stopwords': u'а,без,более,бы,был,была,были,было,быть,в,вам,вас,весь,во,вот,все,всего,всех,вы,где,да,даже,для,до,его,ее,если,есть,еще,же,за,здесь,и,из,или,им,их,к,как,ко,когда,кто,ли,либо,мне,может,мы,на,надо,наш,не,него,нее,нет,ни,них,но,ну,о,об,однако,он,она,они,оно,от,очень,по,под,при,с,со,так,также,такой,там,те,тем,то,того,тоже,той,только,том,ты,у,уже,хотя,чего,чей,чем,что,чтобы,чье,чья,эта,эти,это,я,a,an,and,are,as,at,be,but,by,for,if,in,into,is,it,no,not,of,on,or,such,that,the,their,then,there,these,they,this,to,was,will,with'

        }

        super(CustomElasticsearchBackend, self).__init__(connection_alias,
                                                         **connection_options)


class CustomElasticsearchSearchEngine(ElasticsearchSearchEngine):
    backend = CustomElasticsearchBackend
