import re

from django.contrib.staticfiles.templatetags.staticfiles import static
from django.apps import apps

from django.utils.safestring import mark_safe
from django_jinja import library
from jinja2.filters import do_striptags

from common import thread_locals

import jinja2


@library.global_function
def vt_static(path, **kwargs):
    return static(path, **kwargs)


@library.global_function
def vt_scripts():
    return mark_safe(
        '\n'.join(thread_locals.get_thread_var('_vt_scripts', {}).values()))


@library.global_function
def vt_thumb_margin(image, geometry):
    from sorl.thumbnail.templatetags import thumbnail
    return thumbnail.margin(image, geometry)


@library.global_function
def vt_thumb(*args, **kwargs):
    from sorl.thumbnail import get_thumbnail
    try:
        return get_thumbnail(*args, **kwargs)
    except Exception as e:
        return str(e)


@library.global_function
def vt_tree(qset):
    prev_item = level_0 = None

    for item in qset:
        if level_0 is None:
            level_0 = item.level

        if prev_item:
            prev_item.vt_close = 0

            if prev_item.level < item.level:
                prev_item.vt_open = True
            elif prev_item.level > item.level:
                prev_item.vt_close = prev_item.level - item.level

            yield prev_item

        prev_item = item

    if prev_item:
        prev_item.vt_close = prev_item.level - level_0
        yield prev_item


@library.filter
@library.global_function
def vt_iif(expr, result_true, result_false=''):
    return result_true if expr else result_false


@library.filter
def vt_verbose_name(object, is_plural=False):
    model = type(object)
    return model._meta.verbose_name_plural.title() if is_plural else model._meta.verbose_name.title()


@library.filter
@library.global_function
def vt_safe(obj, prop, default=''):
    if obj is None:
        return default
    if isinstance(obj, dict):
        return obj.get(prop, default)

    return getattr(obj, prop, default)


@library.global_function
def vt_strip_slice(str, num=100):
    return do_striptags(str)[0:num]
