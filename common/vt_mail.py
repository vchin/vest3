import os
import logging
import six

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from email.mime.image import MIMEImage

logger = logging.getLogger('vt_mail')

def mail_send(to, **kwargs):
    """Base partner mail function

    Args:
        to(str or list): Receiver mail.
        message_txt: Default email message text.

    Keyword Args:
        subject: Email subject.
        message_html: HTML message, for new email.
        email_from: Sender email.
        images(list): Embedded email png images.
    """

    subject = kwargs.get('subject', settings.DEFAULT_MAIL_SUBJECT)
    message_txt = kwargs.get('message_txt', None)
    message_html = kwargs.get('message_html', None)

    if not (message_txt or message_html):
        raise Exception('message_txt or message_html required')

    email_from = kwargs.get('email_from', settings.DEFAULT_FROM_EMAIL)
    email_to = [to] if isinstance(to, six.string_types) else to

    msg = EmailMultiAlternatives(subject, message_txt or message_html, email_from, email_to)
    msg.attach_alternative(message_html or message_txt, 'text/html')

    # array of paths to png images
    images = kwargs.get('images', None)

    if images:
        msg.mixed_subtype = 'related'

        for f in images:
            name = os.path.basename(f).lower()
            with open(f, 'rb') as fp:
                mime_img = MIMEImage(fp.read())
                mime_img.add_header('Content-ID', '<{}>'.format(name))
                msg.attach(mime_img)

    try:
        msg.send()
    except Exception as e:
        logger.debug('mail not sent {} {}'.format(to, message_txt))